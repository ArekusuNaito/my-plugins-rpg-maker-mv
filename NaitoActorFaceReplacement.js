/*:
 * @plugindesc Replaces ActorFace with an Walking Animation of your party's characters on Main Menu and Status Menu
 * @author ArekusuNaito


 * @param mainMenuX
 * @desc The X position in the Window. Default: 8
 * @default 8

 * @param mainMenuY
 * @desc The Y position in the Window. Default: 8
 * @default 8

 * @param statusMenuX
 * @desc The X position on Menu Status. Default: 20
 * @default 20

 * @param statusMenuY
 * @desc The Y position on Menu Status. Default: 90
 * @default 90

 * @param actorSpriteWidth
 * @desc The Width of your Actor Sprite. Default: 48
 * @default 48

 * @param actorSpriteHeight
 * @desc The Height of your Actor Sprite. Default: 48
 * @default 48

 * @param numberOfFrames
 * @desc The number of frames your characters has in your spritesheet. Default: 3
 * @default 3

 * @param animationSpeed
 * @desc The sprite's animation speed. The greater, the slower Default: 12
 * @default 12

 * @param patternArray
 * @desc Select how your animation will display the sprites. This select the frames for your animation Default: 1,0,1,2
 * @default 1,0,1,2

 * @param statusMenuScale
 * @desc How much the sprite will be scaled compared to your spritesheet's original size on Status Menu Default: 3
 * @default 3

 * @param mainMenuScale
 * @desc How much the sprite will be scaled compared to your spritesheet's original size on Main Menu Default: 2.5
 * @default 2.5



*/
(function()
{

//=============================================================================
// ** PluginParameters
//=============================================================================
var parameters = PluginManager.parameters('NaitoActorFaceReplacement');
var statusMenuX = Number(parameters['statusMenuX'] || 0);
var statusMenuY = Number(parameters['statusMenuY'] || 0);
var actorSpriteWidth = Number(parameters['actorSpriteWidth'] || 0);
var actorSpriteHeight = Number(parameters['actorSpriteHeight'] || 0);
var numberOfFrames = Number(parameters['numberOfFrames'] || 0);
var animationSpeed = Number(parameters['animationSpeed'] || 0);
var patternArray = (parameters['patternArray'].split(",") || 0);
var statusMenuScale = Number(parameters['statusMenuScale'] || 0);
//Main Menu
var mainMenuX = Number(parameters['mainMenuX'] || 0);
var mainMenuY = Number(parameters['mainMenuY'] || 0);
var mainMenuScale = Number(parameters['mainMenuScale'] || 0);
//

console.log(patternArray);

//=============================================================================
// ** Scene_Status
//=============================================================================

var aliasOnActorChange = Scene_Status.prototype.onActorChange;
Scene_Status.prototype.onActorChange = function()
{
  //Destroy it before changing the actor
    this._statusWindow.destroyAnimatedActorCharacterSprite()
    aliasOnActorChange.call(this)
};

//=============================================================================
// ** Window_Status
//=============================================================================



Window_Status.prototype.drawBlock2 = function(y)
{
    this.createAnimatedActorCharacterSprite(this._actor)
    this.drawBasicInfo(204, y);
    this.drawExpInfo(456, y);
};

Window_Status.prototype.createAnimatedActorCharacterSprite = function(actor)
{
  this.animatedActorCharacter = new AnimatedActorCharacterSprite(actor,statusMenuX,statusMenuY);
  this.addChild(this.animatedActorCharacter);
};

Window_Status.prototype.destroyAnimatedActorCharacterSprite = function(y) {
  this.removeChild(this.animatedActorCharacter)
};




//=============================================================================
// ** AnimatedActorCharacterSprite
//=============================================================================
function AnimatedActorCharacterSprite() {
    this.initialize.apply(this, arguments);
}

AnimatedActorCharacterSprite.prototype = Object.create(Sprite_Base.prototype);
AnimatedActorCharacterSprite.prototype.constructor = AnimatedActorCharacterSprite;

AnimatedActorCharacterSprite.prototype.initialize = function(actor,x,y)
{
  Sprite_Base.prototype.initialize.call(this);
  this.actor = actor
  this.actorIndex = this.actor._characterIndex;
  //Properties
  this.numOfFrames=numberOfFrames;
  this.animSpeed=animationSpeed;
	this._ticker = 0;

  this.patternUpdateIndex=0;
  // this.patternArray = [1,0,1,2]
  this.patternArray = patternArray
  this._pattern = this.patternArray[this.patternUpdateIndex];
  this.x = x;
  this.y = y;
  //
  this.createSprite();
  this._frameWidth=actorSpriteWidth;
  this._frameHeight=actorSpriteHeight;


  this.scale.x = statusMenuScale;
  this.scale.y = statusMenuScale;
  this.updateFrame();
};

AnimatedActorCharacterSprite.prototype.createSprite = function()
{
  this.bitmap = ImageManager.loadCharacter(this.actor._characterName);
  //This doesnt work?? :/ Width=0,Height=0 for some reason
  // this._frameWidth = this.bitmap.width / this.numOfFrames; //48? but its 0
  // this._frameHeight = this.bitmap.height;
  ///////
  this._maxPattern = this.patternArray.length;
  this._tickSpeed = this.animSpeed;
  console.log(this);
  // this.opacity = 0;
}

AnimatedActorCharacterSprite.prototype.update = function()
{
  Sprite_Base.prototype.update.call(this);
	this.updateFrame();
};

AnimatedActorCharacterSprite.prototype.updateFrame = function()
{
    var pw = this._frameWidth;
    var ph = this._frameHeight;
    var sx = this.actorIndex <4 ? (this._pattern * pw)+(this.actorIndex*pw*this.numOfFrames) : (this._pattern * pw)+((this.actorIndex-4)*pw*this.numOfFrames)
    var sy = this.actorIndex <4 ? 0 : this._frameHeight*4


	this.setFrame(sx, sy, pw, ph);

	this._ticker += 1;
	if (this._ticker >= this._tickSpeed) {
		// this._pattern = this._pattern == this._maxPattern ? 0 : this._pattern + 1;
    this._pattern = this.patternArray[this.patternUpdateIndex++];
    if(this.patternUpdateIndex==this.patternArray.length)this.patternUpdateIndex=0

		this._ticker = 0;
	};
};

//=============================================================================
// ** Window_MenuStatus
//=============================================================================

Window_MenuStatus.prototype.drawItemImage = function(index)
{
    var actor = $gameParty.members()[index];
    var rect = this.itemRect(index);
    this.changePaintOpacity(actor.isBattleMember());
    this.drawAnimatedActorCharacter(actor,rect.x+mainMenuX,rect.y+mainMenuY)
    this.changePaintOpacity(true);
};

// ** New Functions
Window_MenuStatus.prototype.drawAnimatedActorCharacter = function(actor,x,y)
{

  this.animatedActorCharacter = new AnimatedActorCharacterSprite(actor,x,y)
  this.animatedActorCharacter.scale.x = mainMenuScale
  this.animatedActorCharacter.scale.y = mainMenuScale;
  this.addChild(this.animatedActorCharacter);

}







})();
